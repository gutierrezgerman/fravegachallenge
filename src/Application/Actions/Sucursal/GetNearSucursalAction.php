<?php
declare(strict_types=1);

namespace App\Application\Actions\Sucursal;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetNearSucursalAction extends SucursalAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
    
       $data = $this->getFormData();
       
       $sucursal = $this->sucursalRepository->findClosest($data->longitud, $data->latitud);
       
       $this->logger->info("Sucursal mas cercana", ['id' =>$sucursal->getId()]);

       return $this->respondWithData($sucursal);

        
    }
}
