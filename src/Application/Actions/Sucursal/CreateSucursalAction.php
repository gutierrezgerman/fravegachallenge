<?php
declare(strict_types=1);

namespace App\Application\Actions\Sucursal;

use Psr\Http\Message\{ResponseInterface as Response, ServerRequestInterface as Request};
use Fig\Http\Message\StatusCodeInterface;
use App\Domain\Sucursal\Sucursal;

class CreateSucursalAction extends SucursalAction
{
    
    
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        
       $data = json_decode($this->request->getBody()->getContents());
       $this->logger->debug(__METHOD__, [$data, json_last_error()]);
       $creada = $this->sucursalRepository->create($data->direccion, $data->longitud, $data->latitud);
       
       $this->logger->info(Sucursal::SUCURSAL_CREATED);    

        return $this->respondWithData(Sucursal::SUCURSAL_CREATED,StatusCodeInterface::STATUS_OK);
    }
}
