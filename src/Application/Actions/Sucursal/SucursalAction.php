<?php
declare(strict_types=1);

namespace App\Application\Actions\Sucursal;

use App\Application\Actions\Action;
use App\Domain\Sucursal\SucursalRepository;
use Psr\Log\LoggerInterface;

abstract class SucursalAction extends Action
{
    /**
     * @var SucursalRepository
     */
    protected $sucursalRepository;

    /**
     * @param LoggerInterface $logger
     * @param SucursalRepository  $sucursalRepository
     */
    public function __construct(LoggerInterface $logger, SucursalRepository $sucursalRepository)
    {
        parent::__construct($logger);
        $this->sucursalRepository = $sucursalRepository;
    }
}
