<?php
declare(strict_types=1);

namespace App\Application\Actions\Sucursal;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ViewSucursalAction extends SucursalAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $sucursalId = (int) $this->resolveArg('id');
        $sucursal = $this->sucursalRepository->findSucursalOfId($sucursalId);

        $this->logger->info("Sucursal con id `${sucursalId}` fue leida.");

        return $this->respondWithData($sucursal);
    }
}
