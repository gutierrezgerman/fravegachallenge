<?php
declare(strict_types=1);

namespace App\Domain\Sucursal;

use App\Domain\DomainException\DomainException;

class SucursalCreateException extends DomainException
{
    public $message = 'Error al tratar de crear sucursal';
}
