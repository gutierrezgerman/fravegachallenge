<?php
declare(strict_types=1);

namespace App\Domain\Sucursal;

use App\Domain\DomainException\DomainRecordNotFoundException;

class SucursalNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'Sucursal inexistente.';
}
