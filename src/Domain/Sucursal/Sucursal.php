<?php
declare(strict_types=1);

namespace App\Domain\Sucursal;

use JsonSerializable;

class Sucursal implements JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string
     */
    private $direccion;

    /**
     * @var float
     */
    private $latitud;

    /**
     * @var float
     */
    private $longitud;
    
    
    const MIN_LONGITUDE_VALUE = -180;
    const MAX_LONGITUDE_VALUE = 180;
    const MIN_LATITUDE_VALUE = -90;
    const MAX_LATITUDE_VALUE = 90;
    const SUCURSAL_CREATED ="Sucursal creada";
    const SUCURSAL_DIRECCION_EXIST ="Ya existe una sucursal con esa direccion";
    const SUCURSAL_OUTOFBOUNDS_ERROR = "Valores fuera del rango [-180,180] para longitud o [-90,90] para latitud";

    /**
     * @param int|null  $id
     * @param string    $direccion
     * @param float    $latitud
     * @param float    $longitud
     */
    public function __construct(?int $id, string $direccion, float $longitud, float $latitud)
    {
        $this->id = $id;
        $this->direccion = $direccion;
        $this->latitud = $latitud;
        $this->longitud = $longitud;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDireccion(): string
    {
        return $this->direccion;
    }

    /**
     * @return float
     */
    public function getLatitud(): float
    {
        return $this->latitud;
    }

    /**
     * @return float
     */
    public function getLongitud(): float
    {
        return $this->longitud;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'direccion' => $this->direccion,
            'longitud' => $this->longitud,
            'latitud' => $this->latitud,
        ];
    }

   /**
    * verifica el valor de longitud no esta dentro de los rangos  -180 180
    * @return bool
    */
    public static function isNotValidSucursalLongitude(float $value):bool{
        return $value < Sucursal::MIN_LONGITUDE_VALUE 
                || $value > Sucursal::MAX_LONGITUDE_VALUE;
        
    }

    /**
    * verifica el valor de latitud no esta dentro de los rangos  -90 90
    * @return bool
    */
    public static function isNotValidSucursalLatitude(float $value) :bool{
        return $value < Sucursal::MIN_LATITUDE_VALUE 
                || $value > Sucursal::MAX_LATITUDE_VALUE;
    }

}
