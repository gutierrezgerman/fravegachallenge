<?php
declare(strict_types=1);
namespace App\Domain\Sucursal;

use App\Domain\DomainException\DomainException;

/**
 * Description of SucursalLocationOutOfBoundsException
 *
 * @author Germán Gutierrez <german.gutierrez@snoopconsulting.com>
 */
class SucursalLocationOutOfBoundsException extends DomainException
{
    public $message = Sucursal::SUCURSAL_OUTOFBOUNDS_ERROR;
}
