# Fravega Challenge 1
======================

Para mejorar la experiencia del cliente, la sucursal default que se ofrece para store pickup debe ser la más
cercana a su ubicación. Para esto, una de las funcionalidades que se necesita es conocer la sucursal más cercana
a un punto dado (latitud y longitud).

Diseñar e implementar un servicio que exponga en su API las operaciones CRUD (únicamente creación y
lectura por id) de la entidad Sucursal y la consulta de sucursal más cercana a un punto dado.

Los campos de dicha entidad son id , dirección , latitud y longitud

## Instalación

```bash
# clone el repositorio a un directorio de su agrado
git clone git@gitlab.com:gutierrezgerman/fravegachallenge.git  [my-checkedout-repository] 
```



Ejecute los siguientes comandos de docker-compose
```bash
cd [my-checkedout-repository]
cd docker
docker-compose build
docker-compose up -d
```

Instalar dependencias con composer 
```
 docker exec -it -w /var/www/html/fravegachallenge  fravega-apache composer install

```


Y ya podes correr la aplicacion

```bash
 curl -H "Content-Type: application/json" -d '{ "direccion":"mi sucursal", "longitud": -30.3434542, "latitud": -54.1231321}'  -X POST http://localhost:8080/sucursal
```

Para ejecutar los tests

```bash

docker exec -it -w /var/www/html/fravegachallenge fravega-apache /bin/bash

root@03470a30e284:/var/www/html/fravegachallenge# DB_HOST=fravega-mysql  DB_DATABASE=fravegatest DB_USERNAME=fravegatest DB_PASSWORD=fravegatest DB_PORT=3306  composer test
exit
```

## documentación de la API


### healthcheck 
<details>

<summary markdown="span">
REQUEST GET /healthcheck
</summary>

```bash 
   curl -H 'Content-Type: application/json;charset=UTF-8'    \
   http://localhost:8080/healthcheck
```
<details>
<summary  markdown="span">RESPONSE</summary>

```json

{
    "statusCode": 200,
    "data": "true"
}
```
</details>

<details>
<summary  markdown="span">RESPONSE CON ERROR</summary>

```json
{
    "statusCode": 503,
    "data": "false"
}
```
</details>

</details> 





### Crear nueva Sucural
<details>

<summary markdown="span">
REQUEST POST /sucursal
</summary>

```bash 
   curl -H 'Content-Type: application/json;charset=UTF-8'    \
   --data '{\
      "direccion":"ddfffdjSSjaaah",  \
      "longitud": -30.3434542, \
      "latitud": 90.000000 \
   }' -X POST 'http://localhost:8080/sucursal
   

```
<details>
<summary  markdown="span">RESPONSE</summary>

```json

{
    "statusCode": 200,
    "data": "Sucursal creada"
}
```
</details>

<details>
<summary  markdown="span">RESPONSE CON ERROR DIRECCIÓN</summary>

```json
{
    "statusCode": 500,
    "error": {
        "type": "SERVER_ERROR",
        "description": "Ya existe una sucursal con esa direccion"
    }
}
```
</details>
<details>
<summary  markdown="span">RESPONSE CON ERROR COORDENADAS</summary>

```json
{
    "statusCode": 500,
    "error": {
        "type": "SERVER_ERROR",
        "description": "Valores fuera del rango [-180,180] para longitud o [-90,90] para latitud"
    }
}
```
</details>

<details>
<summary  markdown="span">RESPONSE CON ERROR CONEXIÓN (común para el resto de los endpoints)</summary>

```json
{
    "statusCode": 500,
    "error": {
        "type": "SERVER_ERROR",
        "description": "No se puede establecer conexión con la base de datos "
    }
}
```
</details>
</details> 



## listado de sucursales

<details>

<summary markdown="span">
REQUEST GET /sucursal
</summary>

```bash 
   curl -H 'Content-Type: application/json;charset=UTF-8'  http://localhost:8080/sucursal
```
<details>
<summary  markdown="span">RESPONSE</summary>

```json
{
    "statusCode": 200,
    "data": [
        {
            "id": int,
            "direccion": string,
            "longitud":float,
            "latitud": float
        },
        {
            "id": int,
            "direccion": string,
            "longitud":float,
            "latitud": float
        },
        {
            "id": int,
            "direccion": string,
            "longitud":float,
            "latitud": float
        }
        
    ]
}
```
</details>


</details> 



## Detalles de sucursal 

<details>

<summary markdown="span">
REQUEST GET /sucursal/{id}
</summary>

```bash 
   curl -H 'Content-Type: application/json;charset=UTF-8'  http://localhost:8080/sucursal/1
```
<details>
<summary  markdown="span">RESPONSE</summary>

```json
{
    "statusCode": 200,
    "data": {
        "id": int,
        "direccion":string, 
        "longitud": float,
        "latitud": float
    }
}
```
</details>
<details>
<summary  markdown="span">RESPONSE CON ERROR</summary>

```json
{
    "statusCode": 404,
    "error": {
        "type": "RESOURCE_NOT_FOUND",
        "description": "Sucursal inexistente."
    }
}
```
</details>

</details> 



## Buscar sucursal cercana 
<details>

<summary markdown="span">
REQUEST POST /sucursal/getnear
</summary>

```bash 
   curl -H 'Content-Type: application/json;charset=UTF-8'    \
   --data '{\
      "longitud": -30.3434542, \
      "latitud": 90.000000 \
   }' -X POST 'http://localhost:8080/sucursal/getnear
   

```
<details>
<summary  markdown="span">RESPONSE</summary>

```json
{
    "statusCode": 200,
    "data": {
        "id": int,
        "direccion":string, 
        "longitud": float,
        "latitud": float
    }
}
```
</details>


</details> 



